import sqlite3
import pandas as pd



# Pregunta 1
# Esto se puede ver a través de la siguiente consulta.
consulta1 = pd.read_sql_query("SELECT id_student, nm_year FROM reg_enrollments,upl_academicperiods WHERE upl_academicperiods.id_code = reg_enrollments.id_academicPeriod AND reg_enrollments.is_active = 1", con)
# Si realizamos
consulta1 = consulta1.drop_duplicates()
# Estamos filtrando los estudiantes que se repiten
# por cada curso que realizaron, por ende
consulta1.value_counts("nm_year")

# O graficamente
consulta1.hist()

# Finalmente, se puede ver que la variación es lineal conforme avanzan los años


# Pregunta 2
# Esto es promedio de ramos inscritos por periodo académico
consulta2 = pd.read_sql_query("SELECT id_student, id_academicPeriod FROM reg_enrollments WHERE is_active = 1", con)
# De forma análoga a lo anteior
consulta2 = consulta2.drop_duplicates()
consulta2.value_counts("id_academicPeriod")

# O graficamente
consulta2.hist()
# El comportamiento es más constante que lo anterior, salvo excepciones.

# Pregunta 3
pd.read_sql_query("SELECT upl_courses.ds_name, COUNT(*) FROM reg_enrollments, upl_courses, upl_enrollment_statuses WHERE reg_enrollments.is_active = 1 AND reg_enrollments.id_course = upl_courses.id_code AND upl_enrollment_statuses.ds_description = \"Reprobado\" GROUP BY upl_courses.ds_name ORDER BY COUNT(*) DESC", con).head(5)
# El top 5 de cursos más reprobados son:
# 1.- Inglés 1    con 1089 reprobraciones.
# 2.- Inglés 2    con 935  reprobraciones.
# 3.- Cálculo II  con 873  reprobraciones.
# 4.- Inglés 4    con 796  reprobraciones.
# 5.- Inglés 3    con 766  reprobraciones.
