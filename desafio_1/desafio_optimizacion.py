import itertools
import pandas as pd
import networkx as nx # Librería de grafos.

def create_edges(days, minimum_distance):
    # Creamos esta funcion
    # que dado una lista de dias
    # nos retorna todos los pares de días
    # que estan separados en al menos
    # una distancia minima fija
    edges = list(itertools.combinations(days,2))
    edges_weighted = []
    
    for tupla in edges:
        distancia = abs(tupla[0]-tupla[1])
        
        if distancia >= minimum_distance:
            edges_weighted.append(tuple((tupla[0],tupla[1],distancia)))
    return edges_weighted

def find_path(lenght_path, initial_nodo, Graph, path = []):
    if path == []:
        path = [initial_nodo]
        
    if lenght_path == 1:
        return path
    
    else:    
        count = 0
        neighbors = list(Graph.neighbors(initial_nodo))[count]
        # Con esto, aseguramos que el camino tenga vecinos distintos siempre
        # Para una mejor ejecución, habría que estudiar si el grafo es tal vez
        # conexo o existen nodos de grado 1, pero dado este caso particular
        # no nos preocupamos de esta situación.
        while neighbors in path:
            count += 1
            neighbors = list(Graph.neighbors(initial_nodo))[count]
    
        return find_path(lenght_path - 1, neighbors, Graph, path + [neighbors] )
        
        
if __name__ == "__main__":
    import time

    start_time = time.time()
    days = [x for x in range(1,41)]
    nm_days = 7
    minimum_distance = 3

    G = nx.Graph()
    G.add_nodes_from(days)
    G.add_weighted_edges_from(create_edges(days, minimum_distance))
    valid_combination = find_path(nm_days, 1, G)
    print(f"Valid combination: {valid_combination}")
    print(f"Total time: {round(time.time() - start_time, 4)} seconds")